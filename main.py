import control as cnt
import numpy as np
from matplotlib import pyplot as plt
import pint

unit = pint.UnitRegistry()

# Motors:
# 2x clusters of 2x CIM
# 150 lb robot
# 4" wheels
# 24" wheelbase

dt = 0.01
dt = 0.001

# Robot 1:
# 5:1 gear ratio (18.53 ft/s free)
ss_ratio = 5

# Robot 2:
# 15:1 low gear ratio
# 5:1 high gear ratio
ds_ratio_low = 15
ds_ratio_high = 5

# Robot 3:
# CVT module with a ratio ranging from 1.5:1 to 1:1.5 going into a gearbox with a ratio of 5:1
# -> total reduction of 7.5:1 to 3.33:1

cvt_dec_max = 1.5/1
cvt_inc_max = 1/1.5

cvt_fixed_ratio = 5

cvt_ratio_lowest = cvt_fixed_ratio * cvt_dec_max
cvt_ratio_highest = cvt_fixed_ratio * cvt_inc_max

numMotors = 2
Vnom = 12.  # V
Wfree = 5310./60 * 2*np.pi  # RPM -> rad/s
Istall = 131.*numMotors  # A
Ifree = 2.7 *numMotors # A
R = Vnom/Istall  # ohm
Tstall = 1.78*32.172445  # ft-lb
Kt = Tstall/Istall  # ft-lb/A
Kv = Wfree/(Vnom-Ifree*R)  # rad/s/V
Ke = 1/Kv / (60/(2*np.pi)) # rad/s/V

L = 2./2  # feet
r = (2./12)  # in->ft
m = 150.  # lb
J = 142.  # lb-ft^2

# ------------------------------------------------------------------------

def getSystem(ratio, dt=dt):
    G = ratio
    C1 = -(G**2*Kt)/(Kv*R*r**2)
    C2 = (G*Kt)/(R*r)

    A = np.array([
        #Lx Lv                 Rx Rv
        [0, 1,                 0, 0                ],
        [0, (1/m+(L**2)/J)*C1, 0, (1/m-(L**2)/J)*C1],
        [0, 0,                 0, 1                ],
        [0, (1/m-(L**2)/J)*C1, 0, (1/m+(L**2)/J)*C1]
    ])

    B = np.array([
        [0,                 0                ],
        [(1/m+(L**2)/J)*C2, (1/m-(L**2)/J)*C2],
        [0,                 0                ],
        [(1/m-(L**2)/J)*C2, (1/m+(L**2)/J)*C2]
    ])

    v_to_rpm = 5310/(5310 * 1/12 * 1/60 * 4*np.pi * 1/ratio)

    C = np.array([
        #Lx Lv Rx Rv
        [1, 0, 0, 0],  # Lx
        [0, 1, 0, 0],  # Lv
        [0, 0, 1, 0],  # Rx
        [0, 0, 0, 1],  # Rv
        [0, 1/2, 0, 1/2],  # v
        [0, -1/L, 0, 1/L],  # w
        [0, -v_to_rpm*Ke/R, 0, 0],  # Il
        [0, 0, 0, -v_to_rpm*Ke/R]  # Ir
    ])

    D = np.array([
        #Vl Vr
        [0, 0],  # Lx
        [0, 0],  # Lv
        [0, 0],  # Rx
        [0, 0],  # Rv
        [0, 0],  # v
        [0, 0],  # w
        [1/R, 0],  # Il
        [0, 1/R],  # Ir
    ])

    return cnt.StateSpace(A, B, C, D).sample(dt)


# Measuring time to get to 90% of free speed

# Single speed
sys = getSystem(ratio=ss_ratio)

state = np.array([0, 0, 0, 0]).reshape((4,1))
input = np.array([12, 12]).reshape((2,1))
y = sys.C @ state + sys.D @ input
log_ss = []
power_usage_ss = 0

t = 0
while y[4,0] < 0.9*Wfree*r*1/ss_ratio:
    t += dt
    state = sys.A @ state + sys.B @ input
    y = sys.C @ state + sys.D @ input
    power_usage_ss += (y[6,0]*input[0,0] + y[7,0]*input[1,0])*dt
    log_ss.append([t, ss_ratio] + [k[0] for k in y.tolist()])


# Shifting drivetrain
sys_high = getSystem(ratio=ds_ratio_high)
sys_low = getSystem(ratio=ds_ratio_low)

state = np.array([0, 0, 0, 0]).reshape((4,1))
input = np.array([12, 12]).reshape((2,1))
y = sys.C @ state + sys.D @ input
log_ds = []
power_usage_ds = 0

t = 0
while y[4,0] < 0.9*Wfree*r*1/ds_ratio_high:
    t += dt

    dx_high = ((sys_high.A @ state + sys_high.B @ input)-state) / dt
    dx_low = ((sys_low.A @ state + sys_low.B @ input)-state) / dt

    dy_high = sys_high.C @ dx_high + sys.D @ input
    dy_low = sys_low.C @ dx_low + sys.D @ input

    dv_high = dy_high[4,0]
    dv_low = dy_low[4,0]


    if dv_high>dv_low:
        ratio = ds_ratio_high
    else:
        ratio = ds_ratio_low

    sys_use = getSystem(ratio)

    state = sys_use.A @ state + sys_use.B @ input
    y = sys_use.C @ state + sys_use.D @ input
    power_usage_ds += (y[6,0]*input[0,0] + y[7,0]*input[1,0])*dt

    log_ds.append([t, ratio] + [k[0] for k in y.tolist()])


# CVT drivetrain

def getCVTRatio(outputSpeedRPM, optim='power'):
    if optim == 'power':
        targetRPM = 2670
        ratio = targetRPM/(outputSpeedRPM+1E-6)
        return np.clip(ratio, cvt_inc_max, cvt_dec_max)
    elif optim == 'eff':
        targetRPM = 4800
        ratio = targetRPM/(outputSpeedRPM+1E-6)
        return np.clip(ratio, cvt_inc_max, cvt_dec_max)

state = np.array([0, 0, 0, 0]).reshape((4,1))
input = np.array([12, 12]).reshape((2,1))
y = sys.C @ state + sys.D @ input
log_cvt = []
power_usage_cvt = 0

t = 0
while y[4,0] < 0.9*Wfree*r*1/cvt_fixed_ratio:
    t += dt
    rpm = y[4,0] * 5310/(5310 * 1/12 * 1/60 * 4*np.pi * 1/cvt_fixed_ratio)
    ratio = getCVTRatio(rpm, optim='power')*cvt_fixed_ratio
    sys_use = getSystem(ratio)
    state = sys_use.A @ state + sys_use.B @ input
    y = sys_use.C @ state + sys_use.D @ input
    power_usage_cvt += (y[6,0]*input[0,0] + y[7,0]*input[1,0])*dt

    log_cvt.append([t, ratio] + [k[0] for k in y.tolist()])


log_ss = np.asarray(log_ss)
log_ds = np.asarray(log_ds)
log_cvt = np.asarray(log_cvt)

t_ss = log_ss[-1,0]
t_ds = log_ds[-1,0]
t_cvt = log_cvt[-1,0]

# ------------------------------------------------------------------------

print("Time to reach 90% of free speed ({} ft/s)".format(round(0.9*Wfree*r*1/cvt_fixed_ratio, 2)))
print("Single speed ({}:1):            {} s".format(ss_ratio, round(t_ss,2)))
print("Double speed ({}:1, {}:1):      {} s".format(ds_ratio_low, ds_ratio_high, round(t_ds,2)))
print("CVT ({}:1 - {}:1 into {}:1): {} s".format(round(cvt_dec_max,2), round(cvt_inc_max,2), round(cvt_fixed_ratio, 2), round(t_cvt,2)))

print("")

print("Total power usage:")
print("SS:  {} J".format(round(power_usage_ss)))
print("DS:  {} J".format(round(power_usage_ds)))
print("CVT: {} J".format(round(power_usage_cvt)))

print("")

print("Average power usage: ")
print("SS:  {} W".format(round(power_usage_ss/t_ss,2)))
print("DS:  {} W".format(round(power_usage_ds/t_ds,2)))
print("CVT: {} W".format(round(power_usage_cvt/t_cvt,2)))


fig, ax1 = plt.subplots()
ax1.plot(log_ss[:,0], log_ss[:,6])
ax1.plot(log_ds[:,0], log_ds[:,6])
ax1.plot(log_cvt[:,0], log_cvt[:,6])
ax1.legend(["Single speed", "Dual speed", "CVT"],loc='lower right')
ax1.set_xlabel("Time (s)")
ax1.set_ylabel("Speed (ft/s)")
#ax2 = ax1.twinx()
#ax2.set_ylabel("Shifting Ratios")
#ax2.plot(log_cvt[:,0],log_cvt[:,1],color="tab:green")
plt.show()
